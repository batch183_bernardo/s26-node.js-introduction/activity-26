// Answer

// 1. require

// 2. http module

// 3. createServer

// 4. writeHead

// 5. terminal

// 6. request.url

let http = require("http");

const port = 3000;

http.createServer((request, response) =>{
	if(request.url == "/login"){
		response.writeHead(200, {"Content-Type": "text/plain"})
		response.end("Welcome to the login page.");
	} else {
		response.writeHead(404, {"Content-Type": "text/plain"})
		response.end("I'm sorry the page you are looking for cannot be found.");
	}
}).listen(port);

console.log(`Server is successfully running`);